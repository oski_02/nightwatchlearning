# NightWacht learning project

## Selelium Server Setup
Selenium Server is a Java application which Nightwatch uses to connect to the various browsers. It runs separately on the machine with the browser you want to test. You will need to have the [Java Development Kit (JDK)](http://www.oracle.com/technetwork/java/javase/downloads/index.html) installed, minimum required version is 7.

Allows to manage multiple browser configurations in one place. To [individualy test some browsers](http://nightwatchjs.org/gettingstarted/#browser-drivers-setup)

* [Download Selenium](http://selenium-release.storage.googleapis.com/index.html) and place it on the computer with the browser you want to test.
A good practice is to create a separate subfolder (e.g. 'bin') and place it there as you might have to download other driver binaries if you want to test multiple browsers.

* 'java -jar bin/selenium-server-standalone-3.8.1.jar'

* [Nightwatch configuration](http://nightwatchjs.org/gettingstarted)

